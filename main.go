package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	bytes, err := ioutil.ReadFile("config.json")
	if err != nil {
		log.Panic(err)
	}
	config := map[string]interface{}{}
	if err := json.Unmarshal(bytes, &config); err != nil {
		log.Panic(err)
	}
	bot, err := tgbotapi.NewBotAPI(config["token"].(string))
	if err != nil {
		log.Panic(err)
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)
	for {
		t := nextInterval()
		log.Println("Next PM in:", t)
		timer := time.NewTimer(t)
		<-timer.C
		bot.Send(tgbotapi.NewMessageToChannel("@thedailydeathreminder", "You will die someday."))
	}
}

var day = time.Hour * 24

func nextInterval() time.Duration {
	// Load everytime to take daylight saving into account
	var loc, _ = time.LoadLocation("Asia/Tehran")
	var _, offset = time.Now().In(loc).Zone()
	hour := rand.Intn(9)
	minute := rand.Intn(60)

	// Generate a random time for tomorrow, between 8am and 4pm, Tehran time.
	return time.Now().
		Truncate(day).
		Add(day + time.Hour*8 + time.Hour*time.Duration(hour) + time.Minute*time.Duration(minute)).
		Sub(time.Now().Add(time.Second * time.Duration(offset)))
}
