FROM golang:1.13-alpine AS build

WORKDIR /app

COPY go.mod .
COPY go.sum .
RUN go mod download

COPY main.go .
RUN go build -o ddr


FROM alpine:latest
COPY --from=build /app/ddr /app
COPY config.json /
ENTRYPOINT [ "/app/ddr" ]